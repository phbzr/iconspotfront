import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {LoginComponent} from './core/auth/login/login.component';
import {SignUpComponent} from './core/auth/sign-up/sign-up.component';
import {AuthenticationComponent} from './core/auth/authentication.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {TokenInterceptor} from './core/auth/token.interceptor';
import {AutofocusDirectiveModule} from './shared/directive/autofocus-directive/autofocus.directive.module';
import {OutsideClickDirective} from './shared/directive/outside-click.directive';
import {DateFormatPipePipe} from './shared/pipes/date-format-pipe.pipe';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Globals} from './shared/globals';
import {IonicStorageModule} from '@ionic/storage';
import {UserFeedComponent} from './modules/user-feed/user-feed.component';
import {HeaderComponent} from './modules/header/header.component';
import {FooterComponent} from './modules/footer/footer.component';
import {MenuComponent} from './modules/menu/menu.component';
import {NotificationsComponent} from './modules/notifications/notifications.component';
import {ChatComponent} from './modules/chat/chat.component';
import {GlobalFeedComponent} from './modules/global-feed/global-feed.component';
import {UserPageComponent} from './modules/user-page/user-page.component';
import {CreatorPageComponent} from './modules/creator-page/creator-page.component';
import {UserPageHeaderComponent} from './modules/user-page-header/user-page-header.component';
import {PostComponent} from './modules/post/post.component';
import {CommentPageComponent} from './modules/comments-page/comment-page.component';
import {NewCommentComponent} from './modules/comments-page/new-comment/new-comment.component';
import {UserPagePostsComponent} from './modules/user-page/user-page-posts/user-page-posts.component';
import {SettingsComponent} from './modules/settings/settings.component';
import {SettingsWelcomeComponent} from './modules/settings/settings-welcome/settings-welcome.component';
import {SettingsSubscriptionComponent} from './modules/settings/settings-subscription/settings-subscription.component';
import {SettingsPersonComponent} from './modules/settings/settings-person/settings-person.component';
import {SettingsPaymentsComponent} from './modules/settings/settings-payments/settings-payments.component';
import {SettingsBankComponent} from './modules/settings/settings-bank/settings-bank.component';
import {ChatListComponent} from './modules/chat/chat-list/chat-list.component';
import {EditSubscriptionComponent} from './modules/settings/settings-subscription/edit-subscription/edit-subscription.component';
import {NewSubscriptionComponent} from './modules/settings/settings-subscription/new-subscription/new-subscription.component';
import {UserRecommendationsComponent} from './modules/global-feed/user-recommendations/user-recommendations.component';
import {PostsGridComponent} from './modules/global-feed/posts-grid/posts-grid.component';
import {SearchComponent} from './modules/global-feed/search/search.component';
import {FilterRowComponent} from './modules/filter-row/filter-row.component';
import {StatsComponent} from './modules/stats/stats.component';
import {CreatorStarterComponent} from './modules/settings/creator-starter/creator-starter.component';
import {PostTapeComponent} from './modules/post-tape/post-tape.component';
import {PostPreviewComponent} from './modules/post-preview/post-preview.component';
import {FollowingComponent} from './modules/following/following.component';
import {SubscriptionComponent} from './modules/subscription/subscription.component';
import {NewPostCameraComponent} from './modules/new-post/new-post-camera/new-post-camera.component';
import {NewPostEditorComponent} from './modules/new-post/new-post-editor/new-post-editor.component';
import {ShortNumberPipe} from './shared/pipes/short-number.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignUpComponent,
    AuthenticationComponent,
    OutsideClickDirective,
    DateFormatPipePipe,
    UserFeedComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    NotificationsComponent,
    ChatComponent,
    GlobalFeedComponent,
    UserFeedComponent,
    UserPageComponent,
    CreatorPageComponent,
    UserPageHeaderComponent,
    PostComponent,
    CommentPageComponent,
    NewCommentComponent,
    UserPagePostsComponent,
    SettingsComponent,
    SettingsWelcomeComponent,
    SettingsSubscriptionComponent,
    SettingsPersonComponent,
    SettingsPaymentsComponent,
    SettingsBankComponent,
    ChatListComponent,
    EditSubscriptionComponent,
    NewSubscriptionComponent,
    UserRecommendationsComponent,
    PostsGridComponent,
    SearchComponent,
    FilterRowComponent,
    StatsComponent,
    CreatorStarterComponent,
    PostTapeComponent,
    PostPreviewComponent,
    FollowingComponent,
    SubscriptionComponent,
    NewPostCameraComponent,
    NewPostEditorComponent,
    ShortNumberPipe,
  ],
  imports: [
    BrowserModule,
    AutofocusDirectiveModule,
    HttpClientModule,
    IonicModule.forRoot(),
    TranslateModule,
    AppRoutingModule,
    ReactiveFormsModule,
    IonicStorageModule.forRoot(),
    FormsModule,
  ],
  providers: [
    Globals,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
