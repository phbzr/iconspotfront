// import {Injectable} from '@angular/core';
// import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';
// import {catchError} from 'rxjs/operators';
// import {throwError} from 'rxjs';
// import {HttpErrorResponse} from '@angular/common/http';
// import {Globals} from '../globals';
// import {ProductService} from '../services/product.service';
//
// @Injectable()
// export class ProductDataResolver implements Resolve<any> {
//
//   private translations: string[];
//
//   constructor(private productService: ProductService,
//               private router: Router,
//               public globals: Globals){
//   }
//
//   resolve(route: ActivatedRouteSnapshot) {
//     const id = route.params.id;
//     return this.productService.getProduct(id).pipe(catchError(async (error) => {
//       if (error instanceof HttpErrorResponse && error.status !== 401) {
//         console.log('exceptions');
//         await this.router.navigate(['/']);
//         return throwError(error);
//       }
//     }));
//   }
// }
