import {Injectable} from '@angular/core';
import {SiteUser} from '../models/site-user';
import {MenuItem} from './models/menu/MenuItem';
import {Post} from '../models/post';
import {Category} from '../models/category';
import {Page} from '../models/page';
import {PostComment} from '../models/post-comment';


@Injectable()
export class Globals {
  // apiUrl = 'http://188.134.72.112:8080';
  // apiUrl = 'http://188.134.72.4:8080';
  apiUrl = 'http://localhost:8080';

  isAuthenticated = false;
  owner: SiteUser;
  serverError: boolean;
  isAdmin = false;
  currentHeader: MenuItem[];

  postExample: Post =
    {
      id: 5,
      owner: this.owner,
      title: 'Рецепт зефирок',
      content: 'Шок раскрыт древний рецепт',
      likesCounter: 56,
      viewsCounter: 173,
      commentsCounter: 37,
      access: 1,
      coverImage: 'https://sun9-56.userapi.com/c858132/v858132179/1d6771/eWN6oZNZf7M.jpg',
      category: new Category(),
      comments: new Page<PostComment>(),
      attachments: [],
      creationDate: new Date(2020, 3, 12, 15, 13, 26),
    };

}
