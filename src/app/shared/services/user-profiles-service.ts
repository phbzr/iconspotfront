import {Injectable} from '@angular/core';
import {SiteUser} from '../../models/site-user';
import {UserProfile} from '../../models/UserProfile';

@Injectable({
  providedIn: 'root'
})
export class UserProfilesService {
  isAdmin(user: SiteUser) {
    let profile: UserProfile;
    for (profile of user.userProfiles) {
      if (profile.id === 150) {
        return true;
      }
    }
    return false;
  }

  isCreator(user: SiteUser) {
    let profile: UserProfile;
    for (profile of user.userProfiles) {
      if (profile.id === 152) {
        return true;
      }
    }
    return false;
  }
}
