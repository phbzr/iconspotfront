import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {MenuItem} from '../../models/menu/MenuItem';
import {Globals} from '../../globals';

@Injectable({
  providedIn: 'root'
})
export class ToolbarService {

  isNewIphone = false;
  newIphone = ['828x1792', '1242x2688', '828x1792', '1125x2436', '1242x2688', '1125x2436'];
  private itemsSource = new Subject<MenuItem[]>();
  items$ = this.itemsSource.asObservable();
  private mainBtnSource = new Subject<MenuItem>();
  mainBtn$ = this.mainBtnSource.asObservable();
  private titleSource = new Subject<string>();
  title$ = this.titleSource.asObservable();
  private headerVisibilitySource = new Subject<boolean>();
  private footerVisibilitySource = new Subject<boolean>();
  headerVisibility$ = this.headerVisibilitySource.asObservable();
  footerVisibility$ = this.footerVisibilitySource.asObservable();
  private searchbarSource = new Subject<MenuItem>();
  searchbar$ = this.searchbarSource.asObservable();

  constructor(private globals: Globals) {
      const w = (window as any);
      const physicalScreenWidth = w.screen.width * w.devicePixelRatio;
      const physicalScreenHeight = w.screen.height * w.devicePixelRatio;
      const currentDevice = physicalScreenWidth + "x" + physicalScreenHeight;

      this.isNewIphone = this.newIphone.filter(i => i === currentDevice).length !== 0;

      if (physicalScreenWidth > 1242 || physicalScreenWidth < 750) {
        this.isNewIphone = false;
      }
  }

  setTitle(title: string) {
    this.titleSource.next(title);
  }

  setMenuItems(items: MenuItem[]) {
    this.itemsSource.next(items);
  }

  setHeaderVisibility(isVisible: boolean) {
    this.headerVisibilitySource.next(isVisible);
  }

  setFooterVisibility(isVisible: boolean) {
    this.footerVisibilitySource.next(isVisible);
  }

  setMainHeaderBtn(item: MenuItem) {
    this.mainBtnSource.next(item);
  }

  setSearchbar(item: MenuItem) {
    this.searchbarSource.next(item);
  }
}
