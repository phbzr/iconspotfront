import {Injectable} from '@angular/core';
import {Globals} from '../globals';
import {HttpClient, HttpParams} from '@angular/common/http';
import {SiteUser} from '../../models/site-user';
import {Page} from '../../models/page';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private globals: Globals, private httpClient: HttpClient, private router: Router) {
  }

  findUsersBy(value: string, page: number): Observable<Page<SiteUser>> {
    let params = new HttpParams();
    params = params.append('value', value);
    return this.httpClient.get<Page<SiteUser>>(this.globals.apiUrl + '/users/search', {
      params: params
    });
  }

  toUser(ssoId: string) {
    this.router.navigateByUrl('/' + ssoId);
  }

  gerRecommendedUsers(page: number): Observable<Page<SiteUser>> {
    return this.httpClient.get<Page<SiteUser>>(this.globals.apiUrl + '');
  }

  getUserBySSoId(ssoId: string): Observable<SiteUser> {
    return this.httpClient.get<SiteUser>(this.globals.apiUrl + '/users/' + ssoId);
  }
}
