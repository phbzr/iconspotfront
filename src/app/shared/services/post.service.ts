import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Globals} from '../globals';
import {Post} from '../../models/post';
import {Observable} from 'rxjs';
import {Page} from '../../models/page';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(public http: HttpClient,
              public globals: Globals,
              private router: Router) {
  }

  getPostById(id: number): Observable<Post> {
    return this.http.get<Post>(this.globals.apiUrl + '/posts/ ' + id);
  }

  addPost(newPost: Post): Observable<any> {
    return this.http.post(this.globals.apiUrl + '/posts/', newPost);
  }

  deletePost(postToDelete: Post): Observable<any> {
    return this.http.delete(this.globals.apiUrl + '/posts/' + postToDelete.id);
  }

  editPost(postToEdit: Post, callback): Observable<any> {
    return this.http.put(this.globals.apiUrl + '/posts/', postToEdit);
  }

  getGlobalFeed(page: number): Observable<Page<Post>> {
    return this.http.get<Page<Post>>(this.globals.apiUrl + '/posts/ ');
  }

  toPost(id: number) {
    this.router.navigateByUrl('/post/' + id);
  }

  getUserFeed(page: number): Observable<Page<Post>> {
    return this.http.get<Page<Post>>(this.globals.apiUrl + '/posts/ ');
  }

  getUserPosts(ssoId: string, page: number): Observable<Page<Post>> {
    let params = new HttpParams();
    params.append('page', page.toString());
    return this.http.get<Page<Post>>(this.globals.apiUrl + '/posts/users/' + ssoId, {
      params: params
    });
  }
}
