import {Injectable} from '@angular/core';
import {Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  constructor() {
  }

  unSubscriber(subscription: Subscription, debug?: string) {
    if (subscription) {
      subscription.unsubscribe();
      if (debug) {
        console.log('unsubscribed: ' + debug);
      }
    }
  }

}
