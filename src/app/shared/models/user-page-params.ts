export class UserPageParams {
  isCurrentUserPage: boolean;
  isFollowed: boolean;
  isSubscribed: boolean;
  isCreator: boolean;
}
