import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {Globals} from './shared/globals';
import {Platform} from '@ionic/angular';
import {ToolbarService} from './shared/services/toolbar/toolbar.service';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  private headerVisibilitySubscription: Subscription;
  isHeaderVisible = true;

  constructor(private router: Router,
              public globals: Globals,
              private platform: Platform,
              private toolbarService: ToolbarService) {
    this.initializeApp();
    this.headerVisibilitySubscription = toolbarService.headerVisibility$.subscribe(
      isVisible => {
        this.isHeaderVisible = isVisible;
      }
    );
  }

  initializeApp() {
    this.platform.ready().then(() => {
    });
  }


}
