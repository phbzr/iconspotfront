import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../shared/globals';
import {ModalController, NavController} from '@ionic/angular';
import {CommentPageComponent} from '../comments-page/comment-page.component';
import {Post} from '../../models/post';
import {DateConverterService} from '../../shared/services/utils/date-converter.service';
import {PostService} from '../../shared/services/post.service';
import {MenuItem} from '../../shared/models/menu/MenuItem';
import {ToolbarService} from '../../shared/services/toolbar/toolbar.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  liked: boolean;
  post: Post;

  closeBtn: MenuItem[] = [
    {
      title: 'Close',
      icon: 'close-outline',
      action: () => {
        this.closePost();
      }
    }
  ];

  constructor(
    public http: HttpClient,
    public globals: Globals,
    public router: Router,
    public modalController: ModalController,
    public dateConverterService: DateConverterService,
    private postService: PostService,
    private activatedRoute: ActivatedRoute,
    private toolbarService: ToolbarService,
    private navController: NavController
  ) {
  }

  ngOnInit(): void {
    const postId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.toolbarService.setMenuItems(this.closeBtn);
    // this.postService.getPostById(postId).subscribe((data: Post) => {
    //   this.post = data;
    // });
    this.globals.postExample.owner = this.globals.owner;
    this.post = this.globals.postExample;
  }

  closePost() {
    this.navController.pop();
  }

  like() {
    this.liked = !this.liked;
  }

  comment() {

  }

  async openComments() {
    const modal = await this.modalController.create({
      component: CommentPageComponent,
      componentProps: {
        data: 'comments'
      }
    });
    return await modal.present();
  }
}
