import {Component, Input} from '@angular/core';
import {Post} from '../../models/post';
import {DateConverterService} from '../../shared/services/utils/date-converter.service';
import {PostService} from '../../shared/services/post.service';

@Component({
  selector: 'app-post-preview',
  templateUrl: './post-preview.component.html',
  styleUrls: ['./post-preview.component.scss']
})
export class PostPreviewComponent {
  @Input() post: Post;

  constructor(public postService: PostService,
              public dateConverterService: DateConverterService) {

  }

}
