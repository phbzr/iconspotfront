import {Component, Input, OnInit} from '@angular/core';
import {Post} from '../../../models/post';
import {Page} from '../../../models/page';

@Component({
  selector: 'app-user-page-posts',
  templateUrl: './user-page-posts.component.html',
  styleUrls: ['./user-page-posts.component.scss']
})
export class UserPagePostsComponent implements OnInit {

  @Input() postsPage: Page<Post>;
  @Input() posts: Array<Post>;

  toPost(number: number) {

  }

  ngOnInit(): void {
    console.log(this.postsPage);
  }

}
