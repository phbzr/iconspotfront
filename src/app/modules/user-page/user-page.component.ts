import {Component, OnInit, ViewChild} from '@angular/core';
import {Globals} from '../../shared/globals';
import {ActivatedRoute} from '@angular/router';
import {UserPageParams} from '../../shared/models/user-page-params';
import {UserProfilesService} from '../../shared/services/user-profiles-service';
import {Post} from '../../models/post';
import {Page} from '../../models/page';
import {UserService} from '../../shared/services/user-service';
import {SiteUser} from '../../models/site-user';
import {PostService} from '../../shared/services/post.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {

  @ViewChild('profileHeaderComponent', {static: false}) profileHeaderComponent;
  @ViewChild('userPagePostsComponent', {static: false}) userPagePostsComponent;

  postsPage: Page<Post>;
  posts: Array<Post>;
  userPageParams: UserPageParams;
  user: SiteUser;

  constructor(public globals: Globals,
              private activatedRoute: ActivatedRoute,
              private userProfilesService: UserProfilesService,
              private userService: UserService,
              private postService: PostService) {
  }

  isOwnerPage(): boolean {
    const pageId = String(this.activatedRoute.snapshot.paramMap.get('ssoId'));
    return this.globals.owner.ssoId === pageId;
  }

  isCreator(): boolean {
    return this.userProfilesService.isCreator(this.user);
  }

  isFollowed() {
  }

  isSubscribed() {
  }

  getPosts(ssoId: string, page: number, callback?: any) {
    this.postService.getUserPosts(ssoId, page).subscribe((data) => {
      this.postsPage = data;
      this.posts = page === 0 ? this.postsPage.content : this.posts.concat(this.postsPage.content);
      if (callback) {
        callback();
      }
    });
  }

  ngOnInit(): void {
    const pageId = String(this.activatedRoute.snapshot.paramMap.get('ssoId'));
    this.userService.getUserBySSoId(pageId).subscribe((data) => {
      this.user = data;
      this.userPageParams = this.constructProfileHeader();
    });
    this.getPosts(pageId, 0);
  }

  constructProfileHeader(): UserPageParams {
    const userPageParams = new UserPageParams();
    userPageParams.isCurrentUserPage = this.isOwnerPage();
    userPageParams.isCreator = this.isCreator();
    // this.userPageParams.isFollowed = ;
    // this.userPageParams.isSubscribed = ;
    return userPageParams;
  }

}
