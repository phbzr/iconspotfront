import {Component} from '@angular/core';
import {ToolbarService} from '../../../shared/services/toolbar/toolbar.service';
import {NavController} from '@ionic/angular';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-post-camera',
  templateUrl: './new-post-camera.component.html',
  styleUrls: ['./new-post-camera.component.scss']
})
export class NewPostCameraComponent {

  constructor(private toolbarService: ToolbarService,
              private navController: NavController,
              private router: Router) {
  }

  ionViewWillEnter() {
    this.toolbarService.setHeaderVisibility(false);
    this.toolbarService.setFooterVisibility(false);
  }

  ionViewWillLeave() {
    this.toolbarService.setHeaderVisibility(true);
    this.toolbarService.setFooterVisibility(true);
  }

  cancel() {
    this.navController.pop();
  }

  next() {
    this.router.navigateByUrl('/new-post-editor');
  }
}
