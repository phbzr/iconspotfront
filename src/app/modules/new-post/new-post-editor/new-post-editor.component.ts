import {Component} from '@angular/core';
import {ToolbarService} from '../../../shared/services/toolbar/toolbar.service';
import {NavController} from '@ionic/angular';
import {Router} from '@angular/router';
import {PostService} from '../../../shared/services/post.service';
import {Post} from '../../../models/post';

@Component({
  selector: 'app-new-post-editor',
  templateUrl: './new-post-editor.component.html',
  styleUrls: ['./new-post-editor.component.scss']
})
export class NewPostEditorComponent {

  isCover: boolean;
  postContent: string;
  postTitle: string;
  postCoverImage = 'https://images.unsplash.com/photo-1508921912186-1d1a45ebb3c1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80';

  constructor(private toolbarService: ToolbarService,
              private navController: NavController,
              private router: Router,
              private postService: PostService) {
  }

  ionViewWillEnter() {
    this.toolbarService.setHeaderVisibility(false);
    this.toolbarService.setFooterVisibility(false);
  }

  ionViewWillLeave() {
    this.toolbarService.setHeaderVisibility(true);
    this.toolbarService.setFooterVisibility(true);
  }

  cancel() {
    this.router.navigateByUrl('/');
  }

  save() {
    const post = new Post();
    post.title = this.postTitle;
    post.content = this.postContent;
    post.coverImage = this.postCoverImage;
    post.access = 1;
    this.postService.addPost(post).subscribe(() => {
      this.router.navigateByUrl('/');
    });
  }

  setCover() {
    this.isCover = !this.isCover;
  }
}
