import {Component} from '@angular/core';
import {ModalController} from '@ionic/angular';


@Component({
  selector: 'app-comment-page',
  templateUrl: './comment-page.component.html',
  styleUrls: ['./comment-page.component.scss']
})
export class CommentPageComponent {

  constructor(public modalCtrl: ModalController) {
  }


  async dismissModal() {
    await this.modalCtrl.dismiss();
  }
}
