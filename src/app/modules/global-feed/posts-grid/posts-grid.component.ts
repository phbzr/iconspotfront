import {Component, Input} from '@angular/core';
import {Post} from '../../../models/post';
import {PostService} from '../../../shared/services/post.service';

@Component({
  selector: 'app-posts-grid',
  templateUrl: './posts-grid.component.html',
  styleUrls: ['./posts-grid.component.scss']
})
export class PostsGridComponent {
  @Input() posts: Array<Post>;

  constructor(public postService: PostService) {
  }

}
