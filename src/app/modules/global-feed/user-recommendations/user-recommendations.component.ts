import {Component, Input, ViewChild} from '@angular/core';
import {SiteUser} from '../../../models/site-user';
import {UserService} from '../../../shared/services/user-service';
import {IonSlides} from '@ionic/angular';
import {Globals} from '../../../shared/globals';

@Component({
  selector: 'app-user-recommendations',
  templateUrl: './user-recommendations.component.html',
  styleUrls: ['./user-recommendations.component.scss']
})
export class UserRecommendationsComponent {
  @Input() users: Array<SiteUser>;
  @ViewChild(IonSlides, {static: true}) slider: IonSlides;

  readonly rootSliderOptions = {
    slidesPerView: 4,
    coverflowEffect: {
      centeredSlides: true,
    }
  };

  init() {

  }

  constructor(public userService: UserService, public globals: Globals) {
  }
}
