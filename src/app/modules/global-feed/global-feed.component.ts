import {Component, OnInit, ViewChild} from '@angular/core';
import {Page} from '../../models/page';
import {SiteUser} from '../../models/site-user';
import {UserService} from '../../shared/services/user-service';
import {Globals} from '../../shared/globals';
import {Post} from '../../models/post';
import {PostService} from '../../shared/services/post.service';

@Component({
  selector: 'app-global-feed',
  templateUrl: './global-feed.component.html',
  styleUrls: ['./global-feed.component.scss']
})
export class GlobalFeedComponent implements OnInit {

  isSearch: boolean = false;
  private searchValue: string;
  foundedUsers: Array<SiteUser>;
  searchPageData: Page<SiteUser>;
  recommendedUsers: Array<SiteUser>;
  recommendedUsersPage: Page<SiteUser>;
  posts: Array<Post>;
  postsPage: Page<Post>;

  constructor(public globals: Globals,
              private userService: UserService,
              private postService: PostService) {
  }

  ngOnInit(): void {
    this.getPosts(0);
    this.getUsers(0);
  }

  getPosts(page: number, callback?: any) {
    this.posts = new Array<Post>();
    for (let i = 14; i > 0; i--) {
      this.posts.push(this.globals.postExample);
    }
    // this.postService.getGlobalFeed(page).subscribe((data: Page<Post>) => {
    //   this.postsPage = data;
    //   this.posts = page === 0 ? this.postsPage.content : this.posts.concat(this.postsPage.content);
    //   if (callback) {
    //     callback();
    //   }
    // });
  }

  getUsers(page: number, callback?: any) {
    this.recommendedUsers = new Array<SiteUser>();
    for (let i = 14; i > 0; i--) {
      this.recommendedUsers.push(this.globals.owner);
    }
    // this.userService.gerRecommendedUsers(page).subscribe((data: Page<SiteUser>) => {
    //   this.recommendedUsersPage = data;
    //   this.recommendedUsers = page === 0 ? this.recommendedUsersPage.content : this.recommendedUsers.concat(this.recommendedUsersPage.content);
    //   if (callback) {
    //     callback();
    //   }
    // });
  }

  searchInputHandler(value: string) {
    this.searchValue = value;
    if (this.searchValue === '') {
      this.isSearch = false;
    } else {
      this.searchUser(value, 0);
    }
  }

  searchUser(value: string, page: number, callback?: any) {
    this.userService.findUsersBy(value, page).subscribe((data: Page<SiteUser>) => {
      this.searchPageData = data;
      this.foundedUsers = page === 0 ? this.searchPageData.content : this.foundedUsers.concat(this.searchPageData.content);
      this.isSearch = true;
      if (callback) {
        callback();
      }
    });
  }
}
