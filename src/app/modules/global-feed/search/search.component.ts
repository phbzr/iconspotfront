import {Component, Input} from '@angular/core';
import {SiteUser} from '../../../models/site-user';
import {Router} from '@angular/router';
import {UserService} from '../../../shared/services/user-service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  @Input() users: Array<SiteUser>;

  constructor(private router: Router, public userService: UserService) {
  }
}
