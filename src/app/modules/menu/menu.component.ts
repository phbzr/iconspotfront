import {Component} from '@angular/core';
import {Globals} from '../../shared/globals';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../core/auth/authentication.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {

  pages = [
    {
      title: 'Messages',
      icon: 'chatbubbles-outline',
      url: '/chat'
    },
    {
      title: 'Following',
      icon: 'people-outline',
      url: '/following'
    },
    {
      title: 'Subscriptions',
      icon: 'star-outline',
      url: '/subscriptions'
    },
    {
      title: 'Statistics',
      icon: 'stats-chart-outline',
      url: '/stats'
    },
    {
      title: 'Settings',
      icon: 'construct-outline',
      url: '/settings/person'
    },
    {
      title: 'Log Out',
      icon: 'log-out-outline',
      action: () => this.logout()
    }
  ];

  constructor(public globals: Globals,
              public router: Router,
              public authService: AuthenticationService) {
  }

  logout() {
    this.authService.logout();
  }

}
