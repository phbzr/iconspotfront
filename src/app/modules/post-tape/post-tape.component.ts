import {Component, Input} from '@angular/core';
import {Post} from '../../models/post';
import {PostService} from '../../shared/services/post.service';

@Component({
  selector: 'app-post-tape',
  templateUrl: './post-tape.component.html',
  styleUrls: ['./post-tape.component.scss']
})
export class PostTapeComponent {
  @Input() posts: Array<Post>;

  constructor(public postService: PostService) {
  }

  loadData($event: any) {
    $event.target.complete();
  }
}
