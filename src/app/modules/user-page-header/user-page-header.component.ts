import {Component, Input} from '@angular/core';
import {Globals} from '../../shared/globals';
import {UserPageParams} from '../../shared/models/user-page-params';
import {Router} from '@angular/router';
import {SiteUser} from '../../models/site-user';

@Component({
  selector: 'app-user-page-header',
  templateUrl: './user-page-header.component.html',
  styleUrls: ['./user-page-header.component.scss']
})
export class UserPageHeaderComponent {
  @Input() user: SiteUser;
  @Input() userPageParams: UserPageParams;

  constructor(public globals: Globals,
              private router: Router) {
  }

  becomeCreator() {
    this.router.navigateByUrl('/settings/subscriptions');
  }

  openUserSettings() {
    this.router.navigateByUrl('/settings/person');
  }
}
