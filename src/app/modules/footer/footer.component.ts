import {Component, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {Globals} from '../../shared/globals';
import {ToolbarService} from '../../shared/services/toolbar/toolbar.service';
import {Subscription} from 'rxjs';
import {SubscriptionService} from '../../shared/services/utils/subscription.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnDestroy {

  footerVisibilitySubscription: Subscription;
  isVisible = true;

  constructor(private router: Router,
              public globals: Globals,
              private toolbarService: ToolbarService,
              private subscriptionService: SubscriptionService) {
    this.footerVisibilitySubscription = toolbarService.footerVisibility$.subscribe(
      isVisible => {
        this.isVisible = isVisible;
      }
    );
  }

  ngOnDestroy(): void {
    this.subscriptionService.unSubscriber(this.footerVisibilitySubscription);
  }

  tabAction(tab: number) {
    switch (tab) {
      case 0:
        this.router.navigateByUrl('/');
        break;
      case 1:
        this.router.navigateByUrl('/global');
        break;
      case 2:
        this.router.navigateByUrl('/new-post');
        break;
      case 3:
        this.router.navigateByUrl('/notifications');
        break;
      case 4:
        this.router.navigateByUrl('/' + this.globals.owner.ssoId);
        break;
      default:
        break;
    }
  }

  showTransferOptions(event) {
    console.log(event);
  }
}
