import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-filter-row',
  templateUrl: './filter-row.component.html',
  styleUrls: ['./filter-row.component.scss']
})
export class FilterRowComponent {
  @Input() colorSchema: string;

}
