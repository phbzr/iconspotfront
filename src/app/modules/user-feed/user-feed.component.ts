import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../shared/globals';
import {PostService} from '../../shared/services/post.service';
import {Post} from '../../models/post';
import {Page} from '../../models/page';
import {ToolbarService} from '../../shared/services/toolbar/toolbar.service';

@Component({
  selector: 'app-user-feed',
  templateUrl: './user-feed.component.html',
  styleUrls: ['./user-feed.component.scss']
})
export class UserFeedComponent implements OnInit {

  posts: Array<Post>;
  userFeedPage: Page<Post>;

  constructor(
    public http: HttpClient,
    public globals: Globals,
    public router: Router,
    public postService: PostService,
    private toolbarService: ToolbarService
  ) {
  }

  ionViewWillEnter() {
    this.toolbarService.setMenuItems([]);
  }

  ngOnInit(): void {
    this.getUserFeed(0);
  }

  private getUserFeed(page: number, callback?: any){
    this.globals.postExample.owner = this.globals.owner;
    this.posts = new Array<Post>();
    for (let i = 14; i > 0; i--) {
      this.posts.push(this.globals.postExample);
    }
    // this.postService.getUserFeed(page).subscribe((data: Page<Post>) => {
    //   this.userFeedPage = data;
    //   this.posts = page === 0 ? this.userFeedPage.content : this.posts.concat(this.userFeedPage.content);
    //   if (callback) {
    //     callback();
    //   }
    // });
  }

  doRefreshContent($event: any) {
    $event.target.complete();
  }


}
