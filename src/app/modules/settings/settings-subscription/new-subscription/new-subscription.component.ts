import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-subscription',
  templateUrl: './new-subscription.component.html',
  styleUrls: ['./new-subscription.component.scss']
})
export class NewSubscriptionComponent {

  constructor(private router: Router) {
  }

  changeSection(section: string) {
    this.router.navigate(['/creator-settings/' + section]);
  }

  saveForm() {

  }

  uploadFile($event) {
    console.log($event.target.files[0]); // outputs the first file
  }
}
