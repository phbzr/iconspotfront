import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-settings-subscription',
  templateUrl: './settings-subscription.component.html',
  styleUrls: ['./settings-subscription.component.scss']
})
export class SettingsSubscriptionComponent {

  constructor(private router: Router) {
  }

  openNewSubscription() {
    this.router.navigate(['/settings/subscriptions/new']);
  }
}
