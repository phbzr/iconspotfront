import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-settings-subscriptions',
  templateUrl: './edit-subscription.component.html',
  styleUrls: ['./edit-subscription.component.scss']
})
export class EditSubscriptionComponent {

  constructor(private router: Router) {
  }

}
