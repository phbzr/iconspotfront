import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-settings-welcome',
  templateUrl: './settings-welcome.component.html',
  styleUrls: ['./settings-welcome.component.scss']
})
export class SettingsWelcomeComponent {

  constructor(private router: Router) {
  }

  changeSection(section: string) {
    this.router.navigate(['/creator-settings/' + section]);
  }

  saveForm() {

  }
}
