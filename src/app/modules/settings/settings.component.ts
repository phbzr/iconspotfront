import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {

  isPerson: boolean;
  isSubscriptions: boolean;
  isPayments: boolean;
  isWelcome: boolean;
  isBank: boolean;
  isCreatorStarter: boolean;
  private routerSub: Subscription;

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.routerSubscription();
  }

  ngOnDestroy(): void {
    this.routerSub.unsubscribe();
  }

  routerSubscription(){
    this.routerSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.falseValues();
        switch (event.url) {
          case '/settings/person':
            this.isPerson = true;
            break;
          case '/settings/bank':
            this.isBank = true;
            break;
          case '/settings/creator-starter':
            this.isCreatorStarter = true;
            break;
          case '/settings/subscriptions':
            this.isSubscriptions = true;
            break;
          case '/settings/payments':
            this.isPayments = true;
            break;
          case '/settings/welcome':
            this.isWelcome = true;
            break;
        }
      }
    });
  }

  falseValues() {
    this.isPerson = false;
    this.isSubscriptions = false;
    this.isPayments = false;
    this.isWelcome = false;
    this.isBank = false;
    this.isCreatorStarter = false;
  }

  changeSection(section: string) {
    this.router.navigate(['/settings/' + section]);
  }

  saveForm() {

  }
}
