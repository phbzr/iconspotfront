import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-settings-person',
  templateUrl: './settings-person.component.html',
  styleUrls: ['./settings-person.component.scss']
})
export class SettingsPersonComponent {

  constructor(private router: Router) {
  }

  changeSection(section: string) {
    this.router.navigate(['/creator-settings/' + section]);
  }

  saveForm() {

  }

  uploadFile($event) {
    console.log($event.target.files[0]); // outputs the first file
  }
}
