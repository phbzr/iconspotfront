import {Component, OnDestroy} from '@angular/core';
import {MenuItem} from '../../shared/models/menu/MenuItem';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {Globals} from '../../shared/globals';
import {ToolbarService} from '../../shared/services/toolbar/toolbar.service';
import {SubscriptionService} from '../../shared/services/utils/subscription.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnDestroy {

  itemsSubscription: Subscription;
  headerVisibilitySubscription: Subscription;
  mainBtnSubscription: Subscription;

  title: string;
  items: MenuItem[];
  isVisible = true;
  mainBtn: MenuItem;

  constructor(
    public router: Router,
    public globals: Globals,
    private toolbarService: ToolbarService,
    private subscriptionService: SubscriptionService
  ) {
    this.itemsSubscription = toolbarService.items$.subscribe(
      items => {
        this.items = items;
        this.globals.currentHeader = this.items;
      }
    );

    this.headerVisibilitySubscription = toolbarService.headerVisibility$.subscribe(
      isVisible => {
        this.isVisible = isVisible;
      }
    );

    this.mainBtnSubscription = toolbarService.mainBtn$.subscribe(
      mainBtn => {
        this.mainBtn = mainBtn;
      }
    );
  }

  ngOnDestroy(): void {
    this.subscriptionService.unSubscriber(this.itemsSubscription);
    this.subscriptionService.unSubscriber(this.headerVisibilitySubscription);
    this.subscriptionService.unSubscriber(this.mainBtnSubscription);
  }

  async applyItem(item: MenuItem, env: any) {

    if (item.action) {
      return item.action(env);
    }

    if (item.path) {
      this.router.navigateByUrl(item.path).then(event => {
        console.log(event ? 'Navigation is successful!' : 'Navigation has failed!');
      });
    }
  }
}
