import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './core/auth/login/login.component';
import {AuthenticationComponent} from './core/auth/authentication.component';
import {SignUpComponent} from './core/auth/sign-up/sign-up.component';
import {UserFeedComponent} from './modules/user-feed/user-feed.component';
import {AuthenticationService} from './core/auth/authentication.service';
import {GlobalFeedComponent} from './modules/global-feed/global-feed.component';
import {UserPageComponent} from './modules/user-page/user-page.component';
import {CreatorPageComponent} from './modules/creator-page/creator-page.component';
import {PostComponent} from './modules/post/post.component';
import {SettingsComponent} from './modules/settings/settings.component';
import {ChatComponent} from './modules/chat/chat.component';
import {NewSubscriptionComponent} from './modules/settings/settings-subscription/new-subscription/new-subscription.component';
import {EditSubscriptionComponent} from './modules/settings/settings-subscription/edit-subscription/edit-subscription.component';
import {NotificationsComponent} from './modules/notifications/notifications.component';
import {StatsComponent} from './modules/stats/stats.component';
import {FollowingComponent} from './modules/following/following.component';
import {SubscriptionComponent} from './modules/subscription/subscription.component';
import {NewPostCameraComponent} from './modules/new-post/new-post-camera/new-post-camera.component';
import {NewPostEditorComponent} from './modules/new-post/new-post-editor/new-post-editor.component';

const routes: Routes = [
  {path: '', component: UserFeedComponent, canActivate: [AuthenticationService]},
  {path: 'login', component: LoginComponent},
  {path: 'auth', component: AuthenticationComponent},
  {path: 'sign-up', component: SignUpComponent},
  {path: 'global', component: GlobalFeedComponent, canActivate: [AuthenticationService]},
  {path: 'chat', component: ChatComponent, canActivate: [AuthenticationService]},
  {path: 'creator-page', component: CreatorPageComponent, canActivate: [AuthenticationService]},
  {path: 'post/:id', component: PostComponent, canActivate: [AuthenticationService]},
  {path: 'settings/:sector', component: SettingsComponent, canActivate: [AuthenticationService]},
  {path: 'settings/subscriptions/new', component: NewSubscriptionComponent, canActivate: [AuthenticationService]},
  {path: 'settings/subscriptions/edit', component: EditSubscriptionComponent, canActivate: [AuthenticationService]},
  {path: 'notifications', component: NotificationsComponent, canActivate: [AuthenticationService]},
  {path: 'stats', component: StatsComponent, canActivate: [AuthenticationService]},
  {path: 'following', component: FollowingComponent, canActivate: [AuthenticationService]},
  {path: 'subscriptions', component: SubscriptionComponent, canActivate: [AuthenticationService]},
  {path: 'new-post', component: NewPostCameraComponent, canActivate: [AuthenticationService]},
  {path: 'new-post-editor', component: NewPostEditorComponent, canActivate: [AuthenticationService]},
  /**
   * IMPORTANT UserPageComponent should be at the end of routes list!
   */
  {path: ':ssoId', component: UserPageComponent, canActivate: [AuthenticationService]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
