import {SiteUser} from './site-user';
import {Photo} from './photo';
import {Category} from './category';

export class Product {
  id: number;
  owner: SiteUser;
  name: string;
  price: number;
  exchange: boolean;
  photos: Array<Photo>;
  locations: string;
  description: string;
  creationdate: Date;
  views: number;
  category: Category;
}
