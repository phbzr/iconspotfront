import {UserProfile} from './UserProfile';

export class SiteUser {
  id: number;
  ssoId: string;
  password: string;
  name: string;
  userProfiles: UserProfile[];
  avatarPath: string;
  profilePhoto: string;
  email?: string;
  phoneNumber?: string;
  authToken?: object;
  subscriptions: number[];
  followersCount?: number;
  viewsCount?: number;
  likesCount?: number;

  constructor(data?: any) {
    Object.assign(this, data);
  }
}
