export class Category {
  id: number;
  title: string;
  icon: string;
}
