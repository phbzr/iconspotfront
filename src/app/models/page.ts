export class Page<T> {
  content: Array<T>;
  last: boolean;
  totalElements: number;
  totalPages: number;
  size: number;
  number: number;
  numberOfElements: number;
  first: boolean;
  empty: boolean;
  maxElements?: number;
  maxPages?: number;
  currentPage?: number;
}
