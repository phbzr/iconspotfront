import {SiteUser} from './site-user';
import {Category} from './category';
import {Page} from './page';
import {PostComment} from './post-comment';
import {PostAttachment} from './post-attachment';

export class Post {
  id: number;
  owner: SiteUser;
  title: string;
  content: string;
  likesCounter: number;
  viewsCounter: number;
  commentsCounter: number;
  access: number;
  coverImage: string;
  category: Category;
  comments: Page<PostComment>;
  attachments: Array<PostAttachment>;
  creationDate: Date;
}
