import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Globals} from '../../../shared/globals';
import {AuthenticationService} from '../authentication.service';
import {SiteUser} from '../../../models/site-user';
import {matchOtherValidator, noCyrillic} from '../../../shared/const/customFormValidators';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss', '../authentication.component.scss']
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup;
  validationData = {
    ssoId: null,
    email: null,
    referrer: null
  };
  errorMsg = '';

  isCheckingEmail = false;
  isCheckingName = false;

  readonly typingTimerInterval = 600;

  ssoIdTypingTimerId;
  emailTypingTimerId;


  constructor(
    private formBuilder: FormBuilder,
    public app: AuthenticationService,
    public globals: Globals,
    public router: Router,
  ) {
    this.signUpForm = this.formBuilder.group({
      ssoId: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [
        Validators.required,
        noCyrillic,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}'),
      ]],
      confirmPassword: [null, [
        Validators.required,
        matchOtherValidator('password')
      ]],
      referrer: [null]
    });
  }

  ngOnInit() {
  }

  parseReferrer(referrer) {
    return referrer !== null ? referrer.toLowerCase().trim() : "";
  }

  // loginWithFb = () => this.faceBookAuthService.accessWithFB(this.parseReferrer(this.signUpForm.value.referrer), true);
  // loginWithGoogle = () => this.googleAuthService.accessWithGoogle(this.parseReferrer(this.signUpForm.value.referrer), true);


  async signUp() {
    if (this.validationData.ssoId == null && this.validationData.email == null) {


      this.signUpForm.value.ssoId = this.signUpForm.value.ssoId.trim();
      this.signUpForm.value.email = this.signUpForm.value.email.trim();
      this.signUpForm.value.referrer = this.parseReferrer(this.signUpForm.value.referrer);

      const error: any = async (msg) => {
        console.log(msg);
      };

      try {
        this.errorMsg = '';

        const newUser = new SiteUser(this.signUpForm.value);

        this.app.register(newUser,
          async () => {
            await this.router.navigateByUrl('/');
          },
          er => {
            this.errorMsg = er.error.message;
            error(er.error.message);
          });
      } catch (er) {
        this.errorMsg = er.message;
        error(er.message);
        console.log(er);
      }
      return false;
    }
  }

  onSsoIdInput(): void {
    clearTimeout(this.ssoIdTypingTimerId);
    this.ssoIdTypingTimerId = setTimeout(() => {
      this.isCheckingName = true;

      this.app.checkRegisterSsoId(this.signUpForm.value.ssoId.toLowerCase())
        .subscribe((res: any) => {
          this.validationData.ssoId = res && res.message ? res.message : null;
          this.isCheckingName = false;
        });
      }, this.typingTimerInterval);
  }

  onEmailInput(): void {
    clearTimeout(this.emailTypingTimerId);
    this.emailTypingTimerId = setTimeout(() => {
      this.isCheckingEmail = true;

      this.app.checkRegisterEmail(this.signUpForm.value.email.toLowerCase())
        .subscribe((res: any) =>  {
          this.validationData.email = res && res.message ? res.message : null;
          this.isCheckingEmail = false;
        });

      }, this.typingTimerInterval);
  }

  back() {
    this.router.navigateByUrl('/auth');
  }
}
