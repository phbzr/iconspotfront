import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {CanActivate, Router} from '@angular/router';
import {Globals} from '../../shared/globals';
import {TokensService} from '../../shared/services/tokens.service';
import {SiteUser} from '../../models/site-user';
import {UserProfile} from '../../models/UserProfile';
import {finalize, tap} from 'rxjs/operators';
import {Tokens} from '../../shared/models/tokens';
import {Storage} from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService implements CanActivate {
  isWrongPassword = false;

  readonly HEADERS = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(public http: HttpClient,
              public router: Router,
              public globals: Globals,
              private storage: Storage,
              private tokensService: TokensService) {
  }

  authenticateWithLowCase(credentials, callback, onError) {
    const params = new HttpParams();
    this.http.post(this.globals.apiUrl + '/accounts/login', {
      username: credentials.username.toLocaleLowerCase(),
      password: credentials.password
    }, {params: params}).subscribe((response: SiteUser) => {
      this.loginProcess(response, credentials.username);
      return callback && callback();
    }, (error: Response) => {
      if (error.status === 401) {
        this.isWrongPassword = true;
        this.globals.isAuthenticated = false;
      } else {
        this.globals.serverError = true;
      }
      return onError && onError(error);
    });
  }

  private loginProcess(response: SiteUser, credentialsName: string) {
    localStorage.setItem('auth-user', JSON.stringify(response));
    if (response.ssoId === credentialsName.toLowerCase() || response.email === credentialsName.toLowerCase()) {
      this.isWrongPassword = false;
      this.tokensService.setTokens(response.authToken);
      this.globals.owner = response as SiteUser;
      this.afterLoginInitiation();
    }
  }

  checkRegisterSsoId(ssoId: string) {
    const options = {
      headers: this.HEADERS
    };

    return this.http.get(`${this.globals.apiUrl}/accounts/ssoid/check?ssoid=${ssoId}`, options);
  }

  checkRegisterEmail(email: string) {
    const options = {
      headers: this.HEADERS
    };

    return this.http.get(`${this.globals.apiUrl}/accounts/email/check?email=${email}`, options);
  }

  register(siteUser: SiteUser, callback, error) {
    const options = {
      headers: this.HEADERS
    };
    this.http.post(this.globals.apiUrl + '/accounts/registration', {
      ssoId: siteUser.ssoId.toLowerCase(),
      email: siteUser.email,
      password: siteUser.password
    }, options).subscribe((response: SiteUser) => {
        this.loginProcess(response, siteUser.ssoId);
        return callback && callback();
      },
      e => {
        return error && error(e);
      });
  }

  canActivate(): boolean {
    let isActive = true;
    if (!this.globals.isAuthenticated) {
      console.log('canActivate ' + this.globals.isAuthenticated);
      this.router.navigate(['auth/']);
      isActive = false;
    }
    return isActive;
  }

  isAdmin(user: SiteUser) {
    let profile: UserProfile;
    for (profile of user.userProfiles) {
      if (profile.id === 150) {
        return true;
      }
    }
    return false;
  }

  afterLoginInitiation() {
    this.globals.isAuthenticated = true;
    this.globals.isAdmin = this.isAdmin(this.globals.owner);
    this.storage.set('auth-user', this.globals.owner);
  }

  logout() {
    this.globals.isAuthenticated = false;
    const params = new HttpParams();
    this.http.post(this.globals.apiUrl + '/accounts/logout', {}, {
      params: params
    }).pipe(
      finalize(async () => {
        await this.logoutProcesses();
      })
    ).subscribe();
  }

  async logoutProcesses() {
    await localStorage.clear();
    await this.storage.set('auth-user', null);
    await this.router.navigate(['login']);
    this.globals.owner = undefined;
  }

  refreshAuthToken(skip?: boolean) {
    const tokens = new Tokens(JSON.parse(localStorage.getItem('tokens')));
    if (!tokens.jwtToken || !tokens.refreshToken || !tokens.ssoId) {
      this.logout();
      return;
    }
    return this.http.post(`${this.globals.apiUrl}/accounts/token`, {
      ssoId: tokens.ssoId,
      jwtToken: tokens.jwtToken,
      refreshToken: tokens.refreshToken,
      skip
    }).pipe(tap((token: any) => token));
  }

  async handleError(er: HttpErrorResponse) {
    console.log(er);
  }
}
