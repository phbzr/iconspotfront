import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', '../authentication.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public app: AuthenticationService,
    public router: Router,
  ) {
    this.loginForm = this.formBuilder.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });
  }

  ngOnInit() {
  }

  // loginWithFb = () => this.facebookAuthService.accessWithFB("", false);
  // loginWithGoogle = () => this.googleAuthService.accessWithGoogle("", false);
  // loginWithAppleID = () => this.appleAuthService.accessWithAppleID("", false);

  async login(hideLoading?: boolean, credential?: any) {


    this.loginForm.value.username = this.loginForm.value.username.trim();


    this.app.authenticateWithLowCase(credential ? credential : this.loginForm.value,
      async () => {
        await this.router.navigateByUrl('/');
      },
      async (error) => {
        if (error.status !== 401) {
          await this.app.handleError(error);
        }
      });
    return false;
  }

  back() {
    this.router.navigateByUrl('/auth');
  }
}
