import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {

  constructor(
    public router: Router) { }

  navigateTo = async (url) => await this.router.navigateByUrl(url);

  ngOnInit() {
  }
}
